package com.example.calculator205150407111003;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class Activity_2 extends AppCompatActivity {

    TextView tv1, tv2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        tv1 = findViewById(R.id.solution_tv);
        tv2 = findViewById(R.id.result_tv);

        Intent in = getIntent();
        Bundle b = in.getExtras();
        String atas = b.getString(MainActivity.operasi);
        String bawah = b.getString(MainActivity.hasil);

        tv1.setText(atas);
        tv2.setText(bawah);
    }
}